Name: boost-python3
Summary: Run-Time component of boost python library for Python 3
Version: 1.53.0
%global version_enc 1_53_0
Release: 27%{?dist}
License: Boost and MIT and Python

%global toplev_dirname boost_%{version_enc}
URL: http://www.boost.org
Group: System Environment/Libraries
Source0: http://downloads.sourceforge.net/boost/%{toplev_dirname}.tar.bz2
Source1: ver.py
Source2: libboost_thread-mt.so

# From the version 13 of Fedora, the Boost libraries are delivered
# with sonames equal to the Boost version (e.g., 1.41.0).
%global sonamever %{version}

BuildRequires: libstdc++-devel
BuildRequires: bzip2-devel
BuildRequires: zlib-devel
BuildRequires: python2-devel
BuildRequires: python%{python3_pkgversion}-devel
BuildRequires: libicu-devel
BuildRequires: chrpath
BuildRequires: gcc-c++

# https://svn.boost.org/trac/boost/ticket/6150
Patch4: boost-1.50.0-fix-non-utf8-files.patch

# Add a manual page for the sole executable, namely bjam, based on the
# on-line documentation:
# http://www.boost.org/boost-build2/doc/html/bbv2/overview.html
Patch5: boost-1.48.0-add-bjam-man-page.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=756005
# https://svn.boost.org/trac/boost/ticket/6131
Patch7: boost-1.50.0-foreach.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=781859
# The following tickets have still to be fixed by upstream.
# https://svn.boost.org/trac/boost/ticket/6408
# https://svn.boost.org/trac/boost/ticket/6410
# https://svn.boost.org/trac/boost/ticket/6413
Patch9: boost-1.53.0-attribute.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=783660
# https://svn.boost.org/trac/boost/ticket/6459 fixed
Patch10: boost-1.50.0-long-double-1.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=828856
# https://bugzilla.redhat.com/show_bug.cgi?id=828857
Patch15: boost-1.50.0-pool.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=909888
Patch16: boost-1.53.0-context.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=984346
# https://svn.boost.org/trac/boost/ticket/7242
Patch17: boost-1.53.0-static_assert-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/8826
Patch22: boost-1.54.0-context-execstack.patch

# https://svn.boost.org/trac/boost/ticket/8844
Patch23: boost-1.54.0-bind-static_assert.patch

# https://svn.boost.org/trac/boost/ticket/8847
Patch24: boost-1.54.0-concept-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/5637
Patch25: boost-1.54.0-mpl-print.patch

# https://svn.boost.org/trac/boost/ticket/8859
Patch26: boost-1.54.0-static_warning-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/8855
Patch27: boost-1.54.0-math-unused_typedef.patch
Patch28: boost-1.54.0-math-unused_typedef-2.patch
Patch29: boost-1.53.0-fpclassify-unused_typedef.patch
Patch30: boost-1.53.0-math-unused_typedef-3.patch

# https://svn.boost.org/trac/boost/ticket/8853
Patch31: boost-1.54.0-tuple-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/8854
Patch32: boost-1.54.0-random-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/8856
Patch33: boost-1.54.0-date_time-unused_typedef.patch
Patch34: boost-1.54.0-date_time-unused_typedef-2.patch

# https://svn.boost.org/trac/boost/ticket/8870
Patch35: boost-1.54.0-spirit-unused_typedef.patch
Patch36: boost-1.54.0-spirit-unused_typedef-2.patch

# https://svn.boost.org/trac/boost/ticket/8871
Patch37: boost-1.54.0-numeric-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/8872
Patch38: boost-1.54.0-multiprecision-unused_typedef.patch

# These are already fixed in 1.54.0+
Patch39: boost-1.53.0-lexical_cast-unused_typedef.patch
Patch40: boost-1.53.0-regex-unused_typedef.patch
Patch41: boost-1.53.0-thread-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/8874
Patch42: boost-1.54.0-unordered-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/8876
Patch43: boost-1.54.0-algorithm-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/8877
Patch44: boost-1.53.0-graph-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/8878
Patch45: boost-1.54.0-locale-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/8879
Patch46: boost-1.54.0-property_tree-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/8880
Patch47: boost-1.54.0-xpressive-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/8881
Patch48: boost-1.54.0-mpi-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/8888
Patch49: boost-1.54.0-python-unused_typedef.patch

# https://svn.boost.org/trac/boost/ticket/9038
Patch51: boost-1.54.0-pool-test_linking.patch

# https://svn.boost.org/trac/boost/ticket/9037
Patch52: boost-1.54.0-thread-cond_variable_shadow.patch

# This was already fixed upstream, so no tracking bug.
Patch53: boost-1.54.0-pool-max_chunks_shadow.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=1018355
Patch54: boost-1.53.0-mpi-version_type.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=1070789
Patch55: boost-1.53.0-buildflags.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=1002578
# https://svn.boost.org/trac/boost/ticket/9065
Patch56: boost-1.54.0-interprocess-atomic_cas32-ppc.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=1134058
# https://svn.boost.org/trac/boost/ticket/7421
Patch57: boost-1.53.0-lexical_cast.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=1298227
# https://github.com/boostorg/asio/pull/23
Patch58: boost-1.53.0-no-ssl3.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=1302120
Patch61: boost-1.53.0-python-libpython_dep.patch
Patch62: boost-1.53.0-python-abi_letters.patch
Patch63: boost-1.53.0-python-test-PyImport_AppendInittab.patch
Patch64: boost-1.53.0-no-rpath.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=1402516
Patch70: boost-1.53-spirit-lexer.patch


%description 
The Boost Python Library is a framework for interfacing Python and
C++. It allows you to quickly and seamlessly expose C++ classes
functions and objects to Python, and viceversa, using no special
tools -- just your C++ compiler.  This package contains run-time
support for Boost Python Library compiled for Python 3.

%package -n boost-python%{python3_pkgversion}
Summary: Shared object symbolic links for Boost.Python 3
Group: System Environment/Libraries

%description -n boost-python%{python3_pkgversion}
The Boost Python Library is a framework for interfacing Python and
C++. It allows you to quickly and seamlessly expose C++ classes
functions and objects to Python, and viceversa, using no special
tools -- just your C++ compiler.  This package contains run-time
support for Boost Python Library compiled for Python 3.

%package -n boost-python%{python3_pkgversion}-devel
Summary: Shared object symbolic links for Boost.Python 3
Group: System Environment/Libraries
Requires: boost-python%{python3_pkgversion}%{?_isa} = %{version}-%{release}
Requires: boost-devel%{?_isa} = %{version}-%{release}

%description -n boost-python%{python3_pkgversion}-devel
Shared object symbolic links for Boost.Python 3

%prep
%setup -q -n %{toplev_dirname}

# Fixes
%patch4 -p1
%patch5 -p1
%patch7 -p2
%patch9 -p1
%patch10 -p1
%patch15 -p0
%patch16 -p1
%patch17 -p1
%patch22 -p1
%patch23 -p1
%patch24 -p1
%patch25 -p0
%patch26 -p1
%patch27 -p1
%patch28 -p0
%patch29 -p1
%patch30 -p1
%patch31 -p0
%patch32 -p0
%patch33 -p0
%patch34 -p1
%patch35 -p1
%patch36 -p1
%patch37 -p1
%patch38 -p1
%patch39 -p1
%patch40 -p1
%patch41 -p1
%patch42 -p1
%patch43 -p1
%patch44 -p1
%patch45 -p1
%patch46 -p1
%patch47 -p1
%patch48 -p1
%patch49 -p1
%patch51 -p1
%patch52 -p1
%patch53 -p1
%patch54 -p1
%patch55 -p1 -b .buildflags
%patch56 -p1
%patch57 -p2
%patch58 -p1
%patch61 -p1
%patch62 -p1
%patch63 -p1
%patch64 -p1
%patch70 -p2

# At least python2_version needs to be a macro so that it's visible in
# %%install as well.
%global python2_version %(/usr/bin/python2 %{SOURCE1})
%global python3_version %(/usr/bin/python3 %{SOURCE1})
%global python3_abiflags %(/usr/bin/python3-config --abiflags)

%build 
: PYTHON2_VERSION=%{python2_version}
: PYTHON3_VERSION=%{python3_version}
: PYTHON3_ABIFLAGS=%{python3_abiflags}

cat >> ./tools/build/v2/user-config.jam << EOF
# There are many strict aliasing warnings, and it's not feasible to go
# through them all at this time.
using gcc : : : <compileflags>"$RPM_OPT_FLAGS -fno-strict-aliasing" ;
%if %{with openmpi} || %{with mpich}
using mpi ;
%endif
# This _adds_ extra python version.  It doesn't replace whatever
# python 2.X is default on the system.
using python : %{python3_version} : /usr/bin/python3 : /usr/include/python%{python3_version}%{python3_abiflags} : : : : %{python3_abiflags} ;
EOF

./bootstrap.sh --with-toolset=gcc --with-icu
sed 's/%%{version}/%{version}/g' %{SOURCE2} > $(basename %{SOURCE2})

# N.B. When we build the following with PCH, parts of boost (math
# library in particular) end up being built second time during
# installation.  Unsure why that is, but all sub-builds need to be
# built with pch=off to avoid this.
#
# The "python=2.*" bit tells jam that we want to _also_ build 2.*, not
# just 3.*.  When omitted, it just builds for python 3 twice, once
# calling the library libboost_python and once libboost_python3.  I
# assume this is for backward compatibility for apps that are used to
# linking against -lboost_python, for when 2->3 transition is
# eventually done.

echo ============================= build serial ==================
./b2 -d+2 -q %{?_smp_mflags} --layout=tagged --with-python \
	variant=release threading=single,multi debug-symbols=on pch=off \
	python=%{python2_version} stage

echo ============================= build Boost.Build ==================
(cd tools/build/v2
 ./bootstrap.sh --with-toolset=gcc)

%check 
:

%install

cd %{_builddir}/%{toplev_dirname}

echo ============================= install serial ==================
./b2 -d+2 -q %{?_smp_mflags} --layout=tagged --with-python \
        --prefix=$RPM_BUILD_ROOT%{_prefix} \
        --libdir=$RPM_BUILD_ROOT%{_libdir} \
        variant=release threading=single,multi debug-symbols=on pch=off \
        python=%{python2_version} install

#remove unpackaged files
rm -rf $RPM_BUILD_ROOT%{_libdir}/libboost_python3-mt.a
rm -rf $RPM_BUILD_ROOT%{_libdir}/libboost_python3.a
rm -rf $RPM_BUILD_ROOT/usr/share/man/man1/bjam.1.gz
rm -rf $RPM_BUILD_ROOT/usr/bin/bjam
rm -rf $RPM_BUILD_ROOT%{_includedir}
rm -rf $RPM_BUILD_ROOT%{_libdir}/libboost_python-*
rm -rf $RPM_BUILD_ROOT%{_libdir}/libboost_python.*

%ldconfig_scriptlets 

%files -n boost-python%{python3_pkgversion}
%license LICENSE_1_0.txt
%{_libdir}/libboost_python3*.so.%{sonamever}

%files -n boost-python%{python3_pkgversion}-devel
%license LICENSE_1_0.txt
%{_libdir}/libboost_python3*.so


%changelog
* Mon Oct 08 2018 Andrea Manzi <andrea.manzi@cern.ch> - 1.53.0-27
- Build and package only boost-python3 libs 

